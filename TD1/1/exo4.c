#include <stdio.h>
#include <stdlib.h>

int exoTD1_1_4(){

	int a, b, c = 0; // We initialize all value to 0

	printf("Enter three integer values :\n");
	scanf("%d", &a);
	scanf("%d", &b);
	scanf("%d", &c);

	// We compute the value in the print function directly so we don't have to have another variable
	printf("The average is : %d", (a+b+c)/3);
	return 0;
}

#include <stdio.h>
#include <stdlib.h>

int exoTD1_1_6(){

	int d = 0;

	printf("Enter the number of days: ");

	scanf("%d", &d);

	// Here we compute the value directly on the functions.
	printf("Years: %d\n", d/365);
	printf("Months: %d\n", (d%365)/30);
	printf("Days: %d", (d%365)%30);

	return 0;
}

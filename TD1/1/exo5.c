#include <stdio.h>
#include <stdlib.h>

int exoTD1_1_5(){

	int h,w = 0;
	printf("Enter the height and the width of the rectangle :\n");

	scanf("%d", &h);
	scanf("%d", &w);

	// We print the result and compute the values directly in the function.
	printf("perimeter: %d; area: %d", h*2 + w*2, h*w);
	return 0;
}

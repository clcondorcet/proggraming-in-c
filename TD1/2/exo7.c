#include <stdio.h>
#include <stdlib.h>
//exoTD1_2_6
int exoTD1_2_7(){

	int a, b;
	char operation;

	printf("Enter a and b: ");
	scanf("%d", &a);
	scanf("%d", &b);

	do{
		printf("Enter an operation between +, -, *, /; enter s to stop and v to enter new values: ");
		scanf("%c", &operation);

		switch (operation) {
			case '+':
				printf("%d + %d = %d\n", a, b, a + b);
				break;
			case '-':
				printf("%d - %d = %d\n", a, b, a - b);
				break;
			case '*':
				printf("%d * %d = %d\n", a, b, a * b);
				break;
			case '/':
				printf("%d / %d = %d\n", a, b, a / b);
				break;
			case 'v':
				printf("Enter a and b: ");
				scanf("%d", &a);
				scanf("%d", &b);
				break;
			default:
				break;
		}
	}while(operation != 's');
	return 0;
}

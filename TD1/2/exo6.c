#include <stdio.h>
#include <stdlib.h>

int exoTD1_2_6(){

	int a, b, c, delta = 0;

	printf("Enter a, b, c of a quadratic equation :\n");
	scanf("%d",&a);
	scanf("%d",&b);
	scanf("%d",&c);

	if(a == 0){
		if(b == 0){
			if (c == 0) printf("R -> x");
			else printf("This is impossible");
		}else{
			printf("x = %f", -c/b);
		}
	}else{
		delta = b*b - 4 * a * c;
		if(delta < 0){
			printf("There is no solution in R");
		}else{
			if(delta == 0){
				printf("x = %f", -b/(2*a));
			}else{
				printf("x1 = %f, x2 = %f", (-b - sqrt(delta)) / (2 * a));
			}
		}
	}

	return 0;
}

#include <stdio.h>
#include <stdlib.h>

int exoTD1_2_1(){

	int a, b = 0;

	printf("Enter two integers: \n");
	scanf("%d", &a);
	scanf("%d", &b);

	if(a == b){
		printf("%d and %d are equals", a, b);
	}else{
		printf("%d and %d are not equals", a, b);
	}

	printf("\n"); // This is just to make a separation between the two text

	// We can also write : because there is only one statment in the if and if the else.
	if(a == b) printf("%d and %d are equals", a, b);
	else printf("%d and %d are not equals", a, b);

	// We can also write:
	/*
	 *
	 * if(CONDITION) CODE TO EXECUTE;
	 * else {
	 *     CODE TO EXECUTE;
	 *     CODE TO EXECUTE;
	 * }
	 *
	 * or
	 *
	 * if(CONDITION) {
	 *     CODE TO EXECUTE;
	 *     CODE TO EXECUTE;
	 * }
	 * else CODE TO EXECUTE;
	 *
	 *
	 * You can see that the {} are just present when there are multiple lines of code but when there is only one line
	 * you don't have to write it. But don't forget the ; at the end !!!
	 */

	return 0;
}

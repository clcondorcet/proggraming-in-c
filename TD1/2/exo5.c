#include <stdio.h>
#include <stdlib.h>

int exoTD1_2_5(){

	int y = 0;

	printf("Enter a year: ");
	scanf("%d",&y);

	if(y%4 == 0 && ((y%100 == 0 && y%400 == 0) || y%100 != 0)){
		printf("%d is a leap year", y);
	}else printf("%d is not a leap year", y);

	return 0;
}

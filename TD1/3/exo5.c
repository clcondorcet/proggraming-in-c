#include <stdio.h>
#include <stdlib.h>
//exoTD1_3_5

int exoTD1_3_5(){

	int W = 2, X = -1, w, x, i, p = 0;
	float U = 4.5;

	printf("Enter p: ");
	scanf("%d", &p);

	for(i = 1; i <= p; i++){
		U = 2 * U - 3;

		w = W;
		x = X;
		W = w + x;
		X = x - w;
	}

	printf("U_%d= %f, W_%d= %d and X_%d= %d", p, U, p, W, p, X);

	return 0;
}

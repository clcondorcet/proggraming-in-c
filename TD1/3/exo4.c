#include <stdio.h>
#include <stdlib.h>
//exoTD1_3_4

int exoTD1_3_4(){

	int x, i, sum = 1;

	printf("Enter x: ");
	scanf("%d", &x);
	i = x;

	while(i > 1){
		sum *= i;
		i--;
	}

	printf("%d! = %d", x, sum);

	return 0;
}

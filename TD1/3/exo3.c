#include <stdio.h>
#include <stdlib.h>
//exoTD1_3_3

int exoTD1_3_3(){

	int a, b, a1, a2, i;

	printf("Enter a and b: ");
	scanf("%d", &a);
	scanf("%d", &b);

	a1 = a;
	a2 = b;

	while(a != b){
		if(a > b){
			i = a;
			a = a - b;
			b = i;
		}else{
			b = b - a;
		}
	}

	printf("gcd(%d,%d) = %d", a1, a2, a);

	return 0;
}

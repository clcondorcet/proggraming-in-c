#include <stdio.h>
#include <stdlib.h>
//exoTD1_3_6

int exoTD1_3_6(){

	int randn = 0, x;

	randn = (rand() + 1) % 10001;

	do{
		printf("Enter a number between 1 and 10000: ");
		scanf("%d", &x);
		if(x != randn){
			if(x < randn){
				printf("The number to guess is greater than %d\n", x);
			}else{
				printf("The number to guess is lower than %d\n", x);
			}
		}
	}while(x != randn);

	printf("Great ! You have found the number %d !", randn);

	return 0;
}

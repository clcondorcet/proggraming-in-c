#include <stdio.h>
#include <stdlib.h>
//exoTD1_3_1_3

int exoTD1_3_1_3(){

	int x, n, i, result = 1;

	printf("Enter x and n: ");
	scanf("%d", &x);
	scanf("%d", &n);

	i = n;

	while(i > 0){
		result *= x;
		i--;
	}

	printf("%d^%d = %d", x, n, result);

	return 0;
}

#include <stdio.h>
#include <stdlib.h>
//exoTD1_3_1_1

int exoTD1_3_1_1(){

	int a, b, i = 0, result = 0;

	printf("Enter a and b: ");
	scanf("%d", &a);
	scanf("%d", &b);

	while(i != b){
		result += a;
		i++;
	}

	printf("%d * %d = %d", a, b, result);

	return 0;
}

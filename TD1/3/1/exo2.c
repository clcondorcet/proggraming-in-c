#include <stdio.h>
#include <stdlib.h>
//exoTD1_3_1_2

int exoTD1_3_1_2(){

	int a, b, i, result = 0;

	printf("Enter a and b: ");
	scanf("%d", &a);
	scanf("%d", &b);

	i = a;

	while((i- b) >= 0){
		i -= b;
		result++;
	}

	printf("%d / %d = %d", a, b, result);

	return 0;
}

#include <stdio.h>
#include <stdlib.h>
//exoTD1_3_2

int exoTD1_3_2(){

	int x, n, sum = 0;

	printf("Enter an number: ");
	scanf("%d", &x);

	for(n = 1; n <= x/2; n++){
		if(x%n == 0){
			sum += n;
		}
	}

	if(x == sum){
		printf("%d is a perfect number !", x);
	}else{
		printf("%d is not perfect number !", x);
	}

	return 0;
}

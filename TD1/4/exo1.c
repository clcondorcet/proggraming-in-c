#include <stdio.h>
#include <stdlib.h>
//exoTD1_4_1

int exoTD1_4_1(){

	int n, i, o;

	printf("How much lines do you want: ");
	scanf("%d", &n);

	for(i = 0; i < n; i++){
		for(o = 0; o < i + 1; o++){
			printf("*");
		}
		printf("\n");
	}
	return 0;
}

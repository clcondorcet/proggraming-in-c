#include "master.h"

int masterMindMain(){
	int list[5], comb[5], r, attempts = 1, win = 0, well;
	random_comb(comb);
	while(attempts <= 20 && win == 0){

		printf("attempts: %d/20\n",attempts);

		do{
			r = read_comb(list);
			if(r == 0){
				printf("There is a problem try again\n");
			}
		}while(r == 0);

		printf("Youre combination: ");
		display_comb(list);

		well = well_placed(comb, list);

		if(well == SIZE){
			win = 1;
		}
		printf(" | ");
		result(wrong_placed(comb, list), well);
		printf("\n");
		attempts++;
	}

	if(win){
		printf("You win !!");
	}else{
		printf("You lose :( the combination was: ");
		display_comb(comb);
	}
	scanf("%d", &r);
}

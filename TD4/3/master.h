#ifndef TD4_3_MASTER_H_
#define TD4_3_MASTER_H_

#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#define SIZE 5

void display_comb(int comb[]);
int read_comb(int comb[]);
void random_comb(int array[]);
int well_placed(int comb[], int propos[]);
int wrong_placed(int comb[], int propos[]);
void result (int x, int y);

#endif /* TD4_3_MASTER_H_ */

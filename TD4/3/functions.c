#include "master.h"

void display_comb(int comb[]){
	int i;
	for(i = 0; i < SIZE; i++){
		printf("%d", comb[i]);
		if(i != SIZE - 1)
			printf(" - ");
	}
}

int read_comb(int comb[]){
	int num, i;
	printf("Enter a combination: ");
	scanf("%d", &num);
	for(i = 0; i < SIZE; i++){
		comb[SIZE - i - 1] = num% 10;
		if(comb[SIZE - i - 1] > 8 || comb[SIZE - i - 1] < 1){
			return 0;
		}
		num = (num - comb[SIZE - i - 1]) / 10;
	}
	if(num > 0)
		return 0;
	return 1;
}

void random_comb(int array[]){
	int i;
	srand((unsigned)time(NULL));
	for(i = 0; i < SIZE; i++){
		array[i] = 1 + (rand() % 8);
	}
}

int well_placed(int comb[], int propos[]){
	int i, s = 0;
	for(i = 0; i < SIZE; i++){
		if(comb[i] == propos[i]) s++;
	}
	return s;
}

int wrong_placed(int comb[], int propos[]){
	int i, o, s = 0, done[SIZE], c = 0;
	for(i = 0; i < SIZE; i++){
		done[i] = 0;
	}
	for(i = 0; i < SIZE; i++){
		if(comb[i] != propos[i]){
			c = 0;
			o = 0;
			while(c == 0 && o < SIZE){
				if(comb[o] != propos[o] && done[o] == 0 && comb[i] == propos[o]){
					s++;
					done[o] = 1;
					c = 1;
				}
				o++;
			}
		}
	}
	return s;
}

void result (int x, int y){
	printf("%d present - %d well placed", x, y);
}

#ifndef TD4_1_FUNCTIONS_H_
#define TD4_1_FUNCTIONS_H_

#include <stdio.h>
#include <stdlib.h>

int scanValue(int a, int b);
int operation(float a, float b, float *p);


#endif /* TD4_1_FUNCTIONS_H_ */

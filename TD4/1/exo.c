#include "functions.h"

void TD4_1exo1(){
	int x;
	x = scanValue(4, 10);
	printf("the value is: %d\n", x);
}

void TD4_1exo2(){
	float a, b, result;
	int r;
	printf("Enter a, and b: ");
	scanf("%f", &a);
	scanf("%f", &b);

	do{
		r = operation(a, b, &result);
		if(r == 0){
			printf("b can't be 0 !\n");
		}else{
			if(r == 2){
				printf("Enter a, and b: ");
				scanf("%f", &a);
				scanf("%f", &b);
			}else{
				if(r != -1){
					printf("result: %f\n", result);
				}
			}
		}
	}while(r != -1);
}

int TD4_1(){
	int exercise = 0;
	do{
		printf("Choose an exercise between 1 and 2; choose 0 to stop: ");
		scanf("%d", &exercise);
		switch(exercise){
			case 1:
				TD4_1exo1();
				break;
			case 2:
				TD4_1exo2();
				break;
			default:
				break;
		}
	}while(exercise != 0);
	return 0;
}

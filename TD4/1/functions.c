#include "functions.h"

int scanValue(int a, int b){
	int r, x;
	do{
		printf("Enter an integer value between %d and %d: ", a, b);
		r = scanf(" %d", &x);
		if(r == 0 || x < a || x > b){
			printf("\nThe value is incorect.\n");
		}
	}while(r == 0 || x < a || x > b);
	return x;
}

float add(float a, float b){
	return a + b;
}

float sub(float a, float b){
	return a - b;
}

float mult(float a, float b){
	return a * b;
}

int divi(float a, float b, float *p){
	if(b == 0){
		return 0;
	}else{
		*p = a / b;
		return 1;
	}
}

int operation(float a, float b, float *p){
	char operation;
	printf("Enter an operation between +, -, *,/, c to change values and others to quit: ");
	fflush(stdin);
	operation = getchar();
	switch (operation) {
		case '+':
			*p = add(a, b);
			break;
		case '-':
			*p = sub(a, b);
			break;
		case '*':
			*p = mult(a, b);
			break;
		case '/':
			if(divi(a, b, p) == 0){
				return 0;
			}
			break;
		case 'c':
			return 2;
		default:
			return -1;
	}
	return 1;
}

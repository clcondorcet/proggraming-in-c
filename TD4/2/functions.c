#include "functions.h"

void test(int x){
	int y;

	printf("func: %d %d", &x, &y);
}

void test2(int o){
	int r;

	printf("func: %d %d", &o, &r);
}

int* test3(){
	int r = 1000;

	printf("func: %d %d", &r, r);
	return &r;
}

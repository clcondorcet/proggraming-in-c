#include "functions.h"

int TD4_2_1(){
	int x = 0, y;
	test(x);
	printf("main: %p %p", &x, &y);
	return 0;
	// we can see that all the adress are different [output: func: 6422272 6422252main: 6422296 6422300]
}

int TD4_2_2(){
	int x = 0, y;
	test2(x);
	printf("main: %p %p", &x, &y);
	return 0;
	//we can see that the adress are the same as before [output: func: 6422272 6422252main: 6422296 6422300]
}

int TD4_2_3(){
	int *p;
	p = test3();
	printf("main: %p %d", p, *p);
	return 0;
	// I can't print the adress that is return.
}

#include "functions.h"

int * create_array(int size){
	int *p;
	p = (int*) malloc(size * sizeof(int));
	return p;
}

void create_array_bis(int size, int **p){
	*p = (int*) malloc(size * sizeof(int));
}

void fill_array(int size, int *p){
	int i;
	printf("- Fill the array -\n");
	for(i = 0; i < size; i++){
		printf("Enter the value at position %d:", i);
		scanf("%d", (p+i));
	}
}

void display_array(int size, int *p){
	int i;
	for(i = 0; i < size; i++){
		 printf("%d : %d\n", i, *(p+i));
	}
}

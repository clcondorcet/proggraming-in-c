#ifndef TD4_4_1_FUNCTIONS_H_
#define TD4_4_1_FUNCTIONS_H_

#include <stdio.h>
#include <stdlib.h>

int * create_array(int size);
void create_array_bis(int size, int **p);
void fill_array(int size, int *p);
void display_array(int size, int *p);

#endif /* TD4_4_1_FUNCTIONS_H_ */

#include "functions.h"

int TD4_4_1(){
	int *p, i;
	printf("Enter the size of the array: ");
	scanf("%d", &i);
	create_array_bis(i, &p);
	fill_array(i, p);
	display_array(i, p);
	return 0;
}

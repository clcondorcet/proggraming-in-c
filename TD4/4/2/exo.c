#include "functions.h"

int TD4_4_2(){
	int iL, iC, **p;
	printf("Enter a line size: ");
	scanf("%d", &iL);
	printf("Enter a column size: ");
	scanf("%d", &iC);
	create_2darray_bis(iL, iC, &p);
	fill_2darray(p, iL, iC);
	disp_2darray(p, iL, iC);
	return 0;
}

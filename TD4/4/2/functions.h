#ifndef TD4_4_1_FUNCTIONS_H_
#define TD4_4_1_FUNCTIONS_H_

#include <stdio.h>
#include <stdlib.h>

int ** create_2darray(int L, int C);
void create_2darray_bis(int L, int C, int ***l);
void fill_2darray(int **p, int L, int C);
void disp_2darray(int **p, int L, int C);

#endif /* TD4_4_1_FUNCTIONS_H_ */

#include "functions.h"

int ** create_2darray(int L, int C){
	int il, **l, *c;

	l = malloc(L * sizeof(int*));

	for(il = 0; il < L; il++){
		c = malloc(C * sizeof(int));
		*(l + il) = c;
	}
	return l;
}

void create_2darray_bis(int L, int C, int ***l){
	int il, *c;

	*l = malloc(L * sizeof(int*));

	for(il = 0; il < L; il++){
		c = malloc(C * sizeof(int));
		*(*l + il) = c;
	}
}

void fill_2darray(int **p, int L, int C){
	int iL, iC, x;
	printf("- Remplir le tableau -\n");
	for(iL = 0; iL < L; iL++){
		printf("Ligne %d.\n", iL + 1);
		for(iC = 0; iC < C; iC++){
			printf("%d: ",iC + 1);
			scanf("%d", &x);
			p[iL][iC] = x;
		}
	}
}

void disp_2darray(int **p, int L, int C){
	int iL, iC;
	for(iL = 0; iL < L; iL++){
		for(iC = 0; iC < C; iC++){
			printf("%d ", p[iL][iC]);
		}
		printf("\n");
	}
}

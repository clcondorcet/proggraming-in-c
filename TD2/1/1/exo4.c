#include <stdio.h>
#include <stdlib.h>
//exoTD2_1_1_4
int exoTD2_1_1_4(){

	int list[10] = {1, 2, -65, 45, 222, 6, 78, -4, 9, 10};
	int i = 0, x = 0;

	printf("Enter the number you want to know the position in the array: ");
	scanf("%d", &x);

	while(i < 10){
		if(list[i] == x){
			printf("%d is on the %d position of the array", x, i+1);
			return 0;
		}
		i++;
	}

	printf("%d is not in the array.", x);
	return 0;
}

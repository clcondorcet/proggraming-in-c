#include <stdio.h>
#include <stdlib.h>
//exoTD2_1_1_5
int exoTD2_1_1_5(){

	int list[10] = {1, 2, -65, 45, 2, 6, 78, 2, 9, 1};
	int i = 0, x = 0, n = 0;

	printf("Enter the number you want to know the positions and occurences in the array: ");
	scanf("%d", &x);

	for(i = 0; i < 10; i++){
		if(list[i] == x){
			printf("\n%d is on the %d position of the array", x, i+1);
			n = n + 1;
		}
	}

	printf("\n%d is present %d times in the array",x, n);
	return 0;
}

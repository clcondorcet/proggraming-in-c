#include <stdio.h>
#include <stdlib.h>
//exoTD2_1_1_2
int exoTD2_1_1_2(){

	int list[10] = {1, 2, -65, 45, 222, 6, 78, -4, 9, 10};
	int min, i = 0;

	min = list[0];

	for(i = 1; i < 10; i++){
		if(min > list[i]){
			min = list[i];
		}
	}

	printf("The minimum of the list is: %d", min);
	return 0;
}

#include <stdio.h>
#include <stdlib.h>
//exoTD2_1_1_6
int exoTD2_1_1_6(){

	//int list[10] = {1, 2, 50, 501, 502, 605, 792, 900, 901, 1000};
	int list[10] = {10, 9, 8, 7, 6, 5, 3, 3, 2, 1};
	int i = 1, x = 0;

	while(i < 10){
		if(x == 0){
			if(list[i] < list[i-1]){
				x = 1;
			}else{
				if(list[i] > list[i-1]){
					x = 2;
				}
			}
		}else{
			if(list[i] != list[i-1]){
				if(x == 1 && list[i] > list[i-1]){
					printf("This array is not sorted");
					return 0;
				}
				if(x == 2 && list[i] < list[i-1]){
					printf("This array is not sorted");
					return 0;
				}
			}
		}
		i++;
	}

	printf("This array is sorted");
	return 0;
}

#include <stdio.h>
#include <stdlib.h>

int exoTD2_1_1_1(){

	int n, i = 0, list[400];

	do{
		printf("Enter the number of values you want to enter: ");
		scanf("%d", &n);
		if(n > 400){
			printf("\n%d is too mutch please enter a number below 400\n", n);
		}
	} while(n > 400);

	for(i = 0; i < n; i++){
		printf("Enter a value: ");
		scanf("%d", &list[i]);
	}

	printf("The list: \n");
	for(i = 0; i < n; i++){
		printf("[%d]: %d\n", i, list[i]);
	}

	return 0;
}

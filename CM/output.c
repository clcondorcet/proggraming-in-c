#include <stdio.h>
#include <stdlib.h>

void createArray(int **array, int size){
	*array = (int *) malloc(size * sizeof(int));
}

int fill(int **array, int psize){
	int n = 0, i, x = 0;
	do {
		printf("Enter the number of values you want to enter: ");
		scanf("%d", &n);
		if(psize < n){
			printf("You can't add more values than the array can contains.\nThe current physical size is %d", psize);
		}
	}
	while(psize < n);
	for(i = 0; i < n; i++){
		scanf("%d", &x);
		*(array + i) = x;
	}
	return n;
}

void display(int *array, int lsize){
	int i;
	for(i = 0; i < lsize; i++){
		printf("[%d]: %d", i, *(array + i));
	}
}

int main2(){


	return 0;
}

#include <stdio.h>
#include <stdlib.h>
//exoTD3_2_1_5
int exoTD3_2_1_5(){

	int i = 0, x = 0, n = 0, *list;

	list = (int *) malloc(10 * sizeof(int));

	*list = 1;
	*(list+1) = 2;
	*(list+2) = -65;
	*(list+3) = 45;
	*(list+4) = 222;
	*(list+5) = 2;
	*(list+6) = 78;
	*(list+7) = 2;
	*(list+8) = 45;
	*(list+9) = 10;

	printf("Enter the number you want to know the positions and occurences in the array: ");
	scanf("%d", &x);

	for(i = 0; i < 10; i++){
		if(*(list+i) == x){
			printf("\n%d is on the %d position of the array", x, i+1);
			n = n + 1;
		}
	}

	printf("\n%d is present %d times in the array",x, n);
	return 0;
}


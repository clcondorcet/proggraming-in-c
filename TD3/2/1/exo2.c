#include <stdio.h>
#include <stdlib.h>
//exoTD3_2_1_2
int exoTD3_2_1_2(){

	int min, i = 0, *list;

	list = (int *) malloc(10 * sizeof(int));

	*list = 1;
	*(list+1) = 2;
	*(list+2) = -65;
	*(list+3) = 45;
	*(list+4) = 222;
	*(list+5) = 6;
	*(list+6) = 78;
	*(list+7) = -4;
	*(list+8) = 9;
	*(list+9) = 10;

	min = *list;

	for(i = 1; i < 10; i++){
		if(min > *(list + i)){
			min = *(list + i);
		}
	}

	printf("The minimum of the list is: %d", min);
	return 0;
}


#include <stdio.h>
#include <stdlib.h>
//exoTD3_2_1_4
int exoTD3_2_1_4(){

	int i = 0, x = 0, *list;

	list = (int *) malloc(10 * sizeof(int));

	*list = 1;
	*(list+1) = 2;
	*(list+2) = -65;
	*(list+3) = 45;
	*(list+4) = 222;
	*(list+5) = 6;
	*(list+6) = 78;
	*(list+7) = -4;
	*(list+8) = 9;
	*(list+9) = 10;

	printf("Enter the number you want to know the position in the array: ");
	scanf("%d", &x);

	while(i < 10){
		if(*(list+i) == x){
			printf("%d is on the %d position of the array", x, i+1);
			return 0;
		}
		i++;
	}

	printf("%d is not in the array.", x);
	return 0;
}


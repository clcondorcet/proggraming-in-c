#include <stdio.h>
#include <stdlib.h>

int exoTD3_2_1_1(){

	int n, i = 0;
	int *p;

	printf("Enter the number of values you want to enter: ");
	scanf("%d", &n);

	// Here I allocate to p n times the integer value so there is n times the place for an integer value.
	p = malloc(n * sizeof(int));

	for(i = 0; i < n; i++){
		printf("Enter a value: ");
		scanf("%d", (p+i));
	}

	printf("The list: \n");
	for(i = 0; i < n; i++){
		printf("[%d]: %d\n", i, *(p + i));
	}

	return 0;
}

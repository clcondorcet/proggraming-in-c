#include <stdio.h>
#include <stdlib.h>
//exoTD3_2_1_8
int exoTD3_2_1_8(){

	int i = 0;
	char *list;
	char x;

	list = (char *) malloc(8 * sizeof(char));

	*list = 'D';
	*(list+1) = 'E';
	*(list+2) = 'C';
	*(list+3) = 'A';
	*(list+4) = 'L';
	*(list+5) = 'A';
	*(list+6) = 'G';
	*(list+7) = 'E';

	x = *list;

	for(i = 0; i < 7; i++){
		*(list + i) = *(list + i + 1);
	}

	*(list + 7) = x;

	for(i = 0; i < 8; i++){
		printf("%c", *(list + i));
	}

	return 0;
}


#include <stdio.h>
#include <stdlib.h>
//exoTD3_2_1_3
int exoTD3_2_1_3(){

	int average = 0, i = 0, *list;

	list = (int *) malloc(10 * sizeof(int));

	*list = 1;
	*(list+1) = 2;
	*(list+2) = -65;
	*(list+3) = 45;
	*(list+4) = 222;
	*(list+5) = 6;
	*(list+6) = 78;
	*(list+7) = -4;
	*(list+8) = 9;
	*(list+9) = 10;

	for(i = 0; i < 10; i++){
		average = average + *(list+i);
	}

	printf("The average of the list is %d", average/10);
	return 0;
}


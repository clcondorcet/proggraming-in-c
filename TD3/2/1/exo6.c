#include <stdio.h>
#include <stdlib.h>
//exoTD3_2_1_6
int exoTD3_2_1_6(){

	int i = 1, x = 0, *list;

	list = (int *) malloc(10 * sizeof(int));

	*list = 1;
	*(list+1) = 2;
	*(list+2) = 3;
	*(list+3) = 4;
	*(list+4) = 5;
	*(list+5) = 6;
	*(list+6) = 7;
	*(list+7) = 90;
	*(list+8) = 190;
	*(list+9) = 200;

	while(i < 10){
		if(x == 0){
			if(list[i] < list[i-1]){
				x = 1;
			}else{
				if(list[i] > list[i-1]){
					x = 2;
				}
			}
		}else{
			if(list[i] != list[i-1]){
				if(x == 1 && list[i] > list[i-1]){
					printf("This array is not sorted");
					return 0;
				}
				if(x == 2 && list[i] < list[i-1]){
					printf("This array is not sorted");
					return 0;
				}
			}
		}
		i++;
	}

	printf("This array is sorted");
	return 0;
}


#include <stdio.h>
#include <stdlib.h>

int a(){

	int n = 0, i = 0, i2 = 0;
	int **p, *c;

	printf("Enter the number of lines: ");
	scanf("%d", &n);

	p = malloc(n * sizeof(int*));

	for(i = 0; i < n; i++){
		c = malloc(n * sizeof(int));
		for(i2 = 0; i2 < n; i2++){
			if(i == i2){
				*(c + i2) = 0;
			}else{
				if(i > i2){
					*(c + i2) = -1;
				}else{
					*(c + i2) = 1;
				}
			}
		}
		*(p + i) = c;
	}

	printf("The list: \n");
	for(i = 0; i < n; i++){
		for(i2 = 0; i2 < n; i2++){
			printf("%d ",*(*(p + i) + i2));
		}
		printf("\n");
	}

	return 0;
}

int b(){

	int n = 0, i = 0, i2 = 0;
	int **p, *c;

	printf("Enter the number of lines: ");
	scanf("%d", &n);

	p = malloc(n * sizeof(int*));

	for(i = 0; i < n; i++){
		c = malloc(n * sizeof(int));
		for(i2 = 0; i2 < n; i2++){
			if(i == i2){
				*(c + i2) = 1;
			}else{
				*(c + i2) = 0;
			}
		}
		*(p + i) = c;
	}

	printf("The list: \n");
	for(i = 0; i < n; i++){
		for(i2 = 0; i2 < n; i2++){
			printf("%d ",*(*(p + i) + i2));
		}
		printf("\n");
	}

	return 0;
}

int c(){

	int n = 0, i = 0, i2 = 0;
	int **p, *c;

	printf("Enter the number of lines: ");
	scanf("%d", &n);

	p = malloc(n * sizeof(int*));

	for(i = 0; i < n; i++){
		c = malloc(n * sizeof(int));
		for(i2 = 0; i2 < n; i2++){
			if(i == n - i2 - 1){
				*(c + i2) = 1;
			}else{
				*(c + i2) = 0;
			}
		}
		*(p + i) = c;
	}

	printf("The list: \n");
	for(i = 0; i < n; i++){
		for(i2 = 0; i2 < n; i2++){
			printf("%d ",*(*(p + i) + i2));
		}
		printf("\n");
	}

	return 0;
}

int d(){

	int n = 0, i = 0, i2 = 0;
	int **p, *c;

	printf("Enter the number of lines: ");
	scanf("%d", &n);

	p = malloc(n * sizeof(int*));

	for(i = 0; i < n; i++){
		c = malloc(n * sizeof(int));
		for(i2 = 0; i2 < n; i2++){
			if(i == i2 || i2 > i){
				*(c + i2) = 1;
			}else{
				*(c + i2) = 0;
			}
		}
		*(p + i) = c;
	}

	printf("The list: \n");
	for(i = 0; i < n; i++){
		for(i2 = 0; i2 < n; i2++){
			printf("%d ",*(*(p + i) + i2));
		}
		printf("\n");
	}

	return 0;
}

int e(){

	int n = 0, i = 0, i2 = 0;
	int **p, *c;

	printf("Enter the number of lines: ");
	scanf("%d", &n);

	p = malloc(n * sizeof(int*));

	for(i = 0; i < n; i++){
		c = malloc(n * sizeof(int));
		for(i2 = 0; i2 < n; i2++){
			if(i2 <= i){
				*(c + i2) = 1;
			}else{
				*(c + i2) = 0;
			}
		}
		*(p + i) = c;
	}

	printf("The list: \n");
	for(i = 0; i < n; i++){
		for(i2 = 0; i2 < n; i2++){
			printf("%d ",*(*(p + i) + i2));
		}
		printf("\n");
	}

	return 0;
}

//exoTD3_2_3_2
int exoTD3_2_3_2(){
	char x;
	printf("Enter a question: ");
	scanf("%c", &x);
	switch (x) {
		case 'a':
			a();
			break;
		case 'b':
			b();
			break;
		case 'c':
			c();
			break;
		case 'd':
			d();
			break;
		case 'e':
			e();
			break;
		default:
			break;
	}
	return 0;
}

#include <stdio.h>
#include <stdlib.h>
//exoTD3_2_3_1
int exoTD3_2_3_1(){

	int n = 0, l = 0, i = 0, i2 = 0;
	int **p, *c;

	printf("Enter the number of columns: ");
	scanf("%d", &n);

	printf("Enter the number of lines: ");
	scanf("%d", &l);

	p = malloc(n * sizeof(int*));

	for(i = 0; i < n; i++){
		c = malloc(l * sizeof(int));
		printf("New Line.\n");
		for(i2 = 0; i2 < l; i2++){
			printf("Enter a value: \n");
			scanf("%d", c+i2);
		}
		*(p + i) = c;
	}

	printf("The list: \n");
	for(i = 0; i < n; i++){
		for(i2 = 0; i2 < l; i2++){
			printf("%d ",*(*(p + i) + i2));
		}
		printf("\n");
	}

	return 0;
}

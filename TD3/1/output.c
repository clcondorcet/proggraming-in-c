#include <stdio.h>
#include <stdlib.h>

int ouputs(){

	int A[9] = {12, 23, 34, 45, 56, 67, 78, 89, 90};
	int *p;
	p=A;

	printf("p: %d, &%d\n", p, &p);
	printf("A: %d, &%d\n", A, &A);
	printf("%d\n", *p + 2);
	printf("%d\n", *(p+2));
	printf("%d\n", &p + 1);
	printf("%d\n", &A[4] - 3);
	printf("%d\n", A + 3);
	printf("%d\n", &A[7] - p);
	printf("%d\n", p + (*p-10));
	printf("%d\n", *(p+*(p+8)-A[7]));
	printf("%d\n", (*(&p))[5]-(*(&p))[2]);
	printf("%d\n", *(*(&p) + 3)*(*(*(&p)+1)-13));

	return 0;
}

#include "Student.h"
#include "Date.h"

void readStudent(Student_t* s)
{
	fflush(stdout);
	fflush(stdin);
	// First name
	printf("Enter the first name of the student: ");
	gets(s->fname);

	// Last name
	printf("Enter the last name of the student: ");
	gets(s->lname);

	// The bdate
	printf("Enter the birth date of the student:\n");
	readDate(&s->bdate);

	// The number of the student
	printf("Enter the number of the student: ");
	scanf("%li", &s->numstud);

	// The average of the student
	printf("Enter the average of the student: ");
	scanf("%lf", &s->average);
}

void displayStudent(Student_t* s)
{
	// Print the first and last names of the student
	printf("Student: ");
	printf("%s %s", s->fname, s->lname);

	// Then the birth date
	printf("\n--> Bith date: ");
	displayDate(&s->bdate);

	// Then the number
	printf("--> Number: %li\n", s->numstud);

	// Then the average
	printf("--> Average: %lf", s->average);
}

Student_t  *createStudent(){
	Student_t * stud = (Student_t *) malloc(sizeof(Student_t));
	return stud;
}

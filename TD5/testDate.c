#include <stdio.h>
#include <stdlib.h>
#include"Date.h"

int mainTestDate()
{
    Date_t *d1;
    Date_t d2={20,4,2020};
    int comp;
    d1=(Date_t *) malloc(sizeof(Date_t));

    readDate(d1);
    readDate(&d2);
    comp=compareDates(d1,&d2);
    if(comp==0)
    {
        displayDate(d1);
        printf("\n est egale a \n");
        displayDate(&d2);
    }
    else
        if(comp==1)
        {
            displayDate(d1);
            printf("\n est ulterieure a \n");
            displayDate(&d2);
        }
        else
        {
            displayDate(d1);
            printf("\n est anterieure a \n");
            displayDate(&d2);
        }

    free(d1);
    return 0;
}

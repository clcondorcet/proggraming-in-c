#ifndef TD5_Date_h
#define TD5_Date_h

#include <stdio.h>

typedef struct
{
    int day;
    int month;
    int year;
}Date_t;

void readDate(Date_t*);
void displayDate(Date_t*);
int compareDates(Date_t*,Date_t*);

#endif /* TD5_Date_h */

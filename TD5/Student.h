#ifndef TD5_STUDENT_H_
#define TD5_STUDENT_H_

#include <stdio.h>
#include "Date.h"

typedef struct
{
	char fname[30];
	char lname[30];
	Date_t bdate;
	long numstud;
	double average;
}Student_t;

void readStudent(Student_t*);
void displayStudent(Student_t*);
Student_t  *createStudent();

#endif /* TD5_STUDENT_H_ */

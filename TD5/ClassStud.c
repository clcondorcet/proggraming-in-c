#include "ClassStud.h"

void readArrayStudents(Student_t ** students, int number)
{
	int i;
	for(i = 0; i < number; i++){
		readStudent(students[i]);
		printf("\n");
	}
}

void displayArrayStudents(Student_t ** students, int number)
{
	int i;
	for(i = 0; i < number; i++){
		printf("\n[%d] ", i+1);
		displayStudent(students[i]);
	}
}

double avArrayStudents(Student_t ** students, int number)
{
	int i;
	double av = 0;
	for(i = 0; i < number; i++){
		 av += students[i]->average;
	}
	return av/number;
}

int bestAverage(Student_t ** students, int number){
	int i, index = 0;
	for(i = 0; i < number; i++){
		if(students[i]->average > students[index]->average){
			index = i;
		}
	}
	return index;
}

int youngestStudent(Student_t ** students, int number){
	int i, index = 0;
	for(i = 0; i < number; i++){
		if(compareDates(&students[i]->bdate, &students[index]->bdate) == -1){
			index = i;
		}
	}
	return index;
}

void readClass(ClassStud_t* class){
	printf("Enter the number of students: ");
	scanf("%d", &class->nbStud);
	class->students = (Student_t**) malloc(class->nbStud * sizeof(Student_t*));
	readArrayStudents(class->students, class->nbStud);
	class->avClass = avArrayStudents(class->students, class->nbStud);
}

void displayClass(ClassStud_t class){
	printf("List of all %d students in the class: ", class.nbStud);
	displayArrayStudents(class.students, class.nbStud);
	printf("\nTotal average of the class: %g", class.avClass);
}

void freeClassStud(ClassStud_t* class){
	int i;
	for(i = 0; i < class->nbStud; i++){
		free(class->students[i]);
	}
	free(class->students);
}

#include "Date.h"

void readDate(Date_t* d)
{
    printf("Donnez une date sous la forme jour/mois/annee\n");
    scanf("%d/%d/%d", &((*d).day),&d->month,&d->year);
}

void displayDate(Date_t* d)
{
    printf("%d/%d/%d\n", d->day,d->month,d->year);
}

int compareDates(Date_t* d1,Date_t* d2)
{
    if(d1->day==d2->day && d1->month==d2->month && d1->year==d2->year)
        return 0;
    if(d1->year >d2->year || (d1->year==d2->year && d1->month >d2->month) ||(d1->year ==d2->year && d1->month ==d2->month && d1->day >d2->day))
        return 1;
    return-1;
}

#ifndef TD5_CLASSSTUD_H_
#define TD5_CLASSSTUD_H_

#include <stdio.h>
#include <stdlib.h>
#include "Student.h"

typedef struct
{
	Student_t ** students;
	int nbStud;
	double avClass;
}ClassStud_t;

void readArrayStudents(Student_t **, int);
void displayArrayStudents(Student_t **, int);
double avArrayStudents(Student_t **, int);
int bestAverage(Student_t **, int);
int youngestStudent(Student_t **, int);
void readClass(ClassStud_t *);
void displayClass(ClassStud_t);
void freeClassStud(ClassStud_t *);

#endif /* TD5_CLASSSTUD_H_ */

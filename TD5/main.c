#include <stdio.h>
#include <stdlib.h>
#include "ClassStud.h"

int main()
{
	int i;
	ClassStud_t class;
	readClass(&class);
	displayClass(class);
	freeClassStud(&class);
	scanf("%d", &i);
	return 0;
}
